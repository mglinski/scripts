#!/usr/bin/env bash

#######################
# Full Stack Setup
# Debian 9 (Stretch), using systemd
# Openresty(1.11), PHP (7.2), Percona MySQL (v5.7), PostgreSql(9.6), Redis(40.), ElasticSearch(5.x), Percona MongoDB(3.4), Memcached(1.4), Oracle Java 8
#
# Sites go in /www like this:
# /www/(site-fqdn)/         Site Root Folder (site-fqdn: website.com)
# /www/*/etc/nginx.conf     Site nginx conf file
# /www/*/ssl/*              Site specific SSL files/secrets (private key, cert, etc)
# /www/*/logs/              Site specific logs (nginx logs, app logs, etc)
# /www/*/public/*           Site web root folder (default webroot, can be overwritten in etc/nginx.conf)
# /www/*/data/*             Site specific misc data folder(sessions, locks, etc)
# /www/*/tmp/*              Site specific tmp folder (scratch folder that can be written to freely)
#######################

echo "----------------------------------"
echo " Setup server from debian minimal "
echo "----------------------------------"
echo
echo "Please choose some of the options below"
echo "to configure individual components."
echo "(case sensitive)"
echo

echo "Would you like to run this with everything turned on? (don't be stupid though...) [y/n]"
read RUN_DEFAULTS ; echo
if [ "$RUN_DEFAULTS" == "y" ]
then
    TIMEZONE=y
    AUTO_UPDATES=y
else

    echo "Configure timezone as UTC? [y/n]"
    read TIMEZONE ; echo

    if [ "$TIMEZONE" == "y" ]; then
        cp /usr/share/zoneinfo/UTC /etc/localtime
        echo "Timezone is now set to UTC"
    fi

    echo "Configure automatic updates? [y/n]"
    read AUTO_UPDATES ; echo

    echo "Perform a apt-get update && upgrade after setting up this machine? [y/n]"
    read POST_UPDATE ; echo
fi

echo "Configure hostname? [y/n]"
read NEW_HOSTNAME ; echo

if [ "$NEW_HOSTNAME" == "y" ] ; then
	echo "Please input your new hostname: "
	read HOSTNAME_URI ; echo

	# backup original hosts file
	cp /etc/hosts /etc/hosts.back

	ORIG_HOSTNAME=`cat /etc/hostname`
	sed -i '' -e  "s/$ORIG_HOSTNAME/HOSTNAME_URI/" /etc/hosts
	cat ${HOSTNAME_URI} > /etc/hostname
fi

echo "Review your choices above and type 'y' to continue..."
read CONT_INSTALL ; echo

if [ "$CONT_INSTALL" == "y" ] ; then
	echo "Continuing install..."
	echo
else
	echo "Exiting installer."
	exit 0
fi

# get everything up to date and install needed packages to load in gpg keys
apt update
apt install -y dirmngr lsb-release apt-transport-https sudo

# Install OpenResty Repo
wget -qO - https://openresty.org/package/pubkey.gpg | apt-key add -
echo "# OpenResty REpo
deb http://openresty.org/package/debian $(lsb_release -sc) openresty" > /etc/apt/sources.list.d/openresty.list

# Install PHP Repo
apt-key adv --keyserver keys.gnupg.net --recv-keys AC0E47584A7A714D
echo "# PHP Repos
deb http://packages.sury.org/php stretch main
deb-src http://packages.sury.org/php stretch main" > /etc/apt/sources.list.d/sury-php-stretch.list

# Install Redis Repo
apt-key adv --keyserver keys.gnupg.net --recv-keys C7917B12
echo "# Redis Repos
deb http://ppa.launchpad.net/chris-lea/redis-server/ubuntu artful main
deb-src http://ppa.launchpad.net/chris-lea/redis-server/ubuntu artful main" > /etc/apt/sources.list.d/chris-lea-redis-server-stretch.list

# Install ElasticSearch Repo
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "# Elastic Search
deb https://artifacts.elastic.co/packages/6.x/apt stable main
deb-src https://artifacts.elastic.co/packages/6.x/apt stable main" > /etc/apt/sources.list.d/elastic-6.x.list


#  Install YARN Repo
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

#  Install Percona Repo Manually
echo "###
deb http://repo.percona.com/apt stretch main
deb-src http://repo.percona.com/apt stretch main
###" | sudo tee /etc/apt/sources.list.d/percona-release.list

# Install Java 8 JRE Package Repo
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

echo "# Java JRE Repos
deb http://ppa.launchpad.net/webupd8team/java/ubuntu artful main
deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu artful main" > /etc/apt/sources.list.d/webupd8team-java.list


# Array of system services to enable after everything is done
SYSTEM_SERVICES=()

# Increase hashing rounds on system passwords ))))))))))
sed -i -e 's/sha512$/sha512 rounds=65535\n/g' /etc/pam.d/common-password

# Download cacert.pem file from haxxe
wget -O /etc/cacert.pem http://curl.haxx.se/ca/cacert.pem

# Initial system update and package upgrade
apt-get update
apt-get upgrade -y

# Install Percona Repos (MySQL)
wget https://repo.percona.com/apt/percona-release_0.1-4.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-4.$(lsb_release -sc)_all.deb


# Enable unattended debian-security upgrades
apt-get install -y unattended-upgrades bsd-mailx

# install needed base packages
apt-get install -y \
	build-essential \
	fail2ban \
	git curl sudo \
	logrotate \
	libreadline6-dev \
	libncurses5-dev \
	libpcre++-dev \
	libssl-dev \
	libgeoip-dev \
	libxml2-dev \
	libxslt1-dev \
	libgd2-xpm-dev \
	libperl-dev \
	zlib1g-dev \
	libpcre3 \
	libpcre3-dev \
	libgoogle-perftools-dev \
	golang \
	cmake \
	libgd-dev \
	libbz2-dev \
	unzip \
	geoip-database-extra \
	geoip-database \
	geoip-bin \
	libgeoip-dev \
	google-perftools \
    libjemalloc-dev \
    gnupg \
    libcurl4-openssl-dev \
    libperl-dev \
    libgd-dev


# Install certbot for letsencrypt and supervisor
apt-get install -y certbot supervisor

# Install OpenResty logrotate config
echo '/var/log/openresty/*.log {
        daily
        missingok
        rotate 52
        compress
        delaycompress
        notifempty
        create 640 openresty adm
        sharedscripts
        postrotate
                [ -f /var/run/openresty.pid ] && kill -USR1 `cat /var/run/openresty.pid`
        endscript
}
' > /etc/logrotate.d/openresty

# Install IoJS 3.x and NPM
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt-get install -y nodejs

# install other services
apt-get install -y redis-server memcached

# Add new system services to processing list
SYSTEM_SERVICES+=('redis-server')
SYSTEM_SERVICES+=('memcached')

# Install OpenResty Group
if ! getent group openresty >/dev/null; then
   addgroup --system openresty >/dev/null
fi

# Creating openresty user if it does not exit
if ! getent passwd openresty >/dev/null; then
    adduser \
        --system \
        --disabled-login \
        --ingroup openresty \
        --no-create-home \
        --home /nonexistent \
        --gecos "openresty user" \
        --shell /bin/false \
        openresty  >/dev/null
fi

# Move into root home dir for downloads and building
cd /root

apt -y install openresty

# add to system services loop
SYSTEM_SERVICES+=('openresty')

# Install PHP + Modules
apt-get install -y php7.2 php-dev php-cli php-cgi php-curl php-common php-bz2 php-bcmath php-fpm php-json \
    php7.2-opcache php-mbstring php-xml php-mysql php-pgsql php-sqlite3 php-intl php-gmp php-igbinary \
    php-memcached php-msgpack php-odbc php-redis php-zip php-sodium php-imagick

# add to system services loop
SYSTEM_SERVICES+=('php7.2-fpm')

# Install composer globally
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
cp composer.phar /usr/bin/composer

# Install PHP OpCache Settings
cat > /etc/php/7.2/mods-available/opcache.ini <<"ZOA"
; configuration for php ZendOpcache module
; priority=05
zend_extension=opcache.so
opcache.enable=1
opcache.save_comments=1
opcache.enable_file_override=1
opcache.memory_consumption=512
opcache.interned_strings_buffer=256
opcache.max_accelerated_files=512000
opcache.revalidate_freq=0
opcache.validate_timestamps=0
opcache.fast_shutdown=1
opcache.enable_cli=0
ZOA

# Set PHP.ini Settings for CLI
cat >> /etc/php/7.2/fpm/php.ini <<"ZOB"

max_execution_time = 3000
max_input_time = 6000
expose_php = Off
short_open_tag = Off
zlib.output_compression = Off
implicit_flush = Off
memory_limit = 512M
error_reporting = E_ALL
display_errors = Off
log_errors = On
error_log = php_errors.log
post_max_size = 2G
cgi.force_redirect = 1
cgi.fix_pathinfo = 0
upload_max_filesize = 2G
max_file_uploads = 10
date.timezone = UTC
#error_log = __php-fpm.log
ZOB

# Set PHP.ini Settings for FPM
cat >> /etc/php/7.1/cli/php.ini <<"ZOB1"

max_execution_time = 3000
max_input_time = 6000
expose_php = Off
short_open_tag = Off
zlib.output_compression = Off
implicit_flush = Off
memory_limit = 512M
error_reporting = E_ALL
display_errors = Off
log_errors = On
error_log = php_errors.log
post_max_size = 2G
cgi.force_redirect = 1
cgi.fix_pathinfo = 0
upload_max_filesize = 2G
max_file_uploads = 10
date.timezone = UTC
#error_log = __php-fpm.log
ZOB1

# Change listen mode in php-fpm socket
sed -i 's/^;listen.mode = */listen.mode = 0666/' /etc/php/7.2/fpm/pool.d/www.conf

# Set Openresty fastcgi_params file
cat > /usr/local/openresty/nginx/conf/fastcgi_params <<"ZOC"

fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;
fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
fastcgi_param  PATH_INFO          $fastcgi_script_name;
#fastcgi_param   PATH_TRANSLATED $document_root$fastcgi_path_info;

fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;
fastcgi_param  HTTPS              $https if_not_empty;

fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;

fastcgi_connect_timeout 300;
fastcgi_send_timeout 300;
fastcgi_read_timeout 300;
fastcgi_buffer_size 32k;
fastcgi_buffers 32 16k;
fastcgi_busy_buffers_size 256k;
fastcgi_temp_file_write_size 256k;
fastcgi_intercept_errors on;

# PHP only, required if PHP was built with --enable-force-cgi-redirect
#fastcgi_param  REDIRECT_STATUS    200;
ZOC

# Set Openresty main config file
cat > /usr/local/openresty/nginx/conf/nginx.conf <<"ZOE"
###########################
##   Openresty v1.13.x   ##
##     ~All~ Features    ##
###########################

##/ Main thread settings \##
user  openresty;
worker_processes  auto;
worker_priority      15; 
worker_rlimit_nofile 7000000;

##/ PCRE JIT compiler for regex \##
pcre_jit                 on;

##/ FileRead Threadpool \##
thread_pool mainpool threads=632 max_queue=65536; # added in 1.7.11

## Main event loop \##
events {
    use epoll;
    worker_connections  10240;
}

##/ Global HTTP Options \##
http {
    include       mime.types;
    default_type  application/octet-stream;

    ##/ SSL: Globally Mitigate Poodle Attacks \##
    ssl_protocols                     TLSv1.1 TLSv1.2;

    ##/ Cache \##
    add_header                        Cache-Control "public, max-age=3153600";
    client_max_body_size              64M;
    client_body_temp_path             /var/cache/openresty 1 2;
    open_file_cache                   max=1000 inactive=2h;
    open_file_cache_errors            on;
    open_file_cache_min_uses          1;
    open_file_cache_valid             1h;

    ##/ Timeouts \##
    send_timeout                      5;
    keepalive_timeout                 5 5;
    client_body_timeout               5;
    client_header_timeout             5;

    ##/ OpenResty \##
    variables_hash_max_size           4096;
    variables_hash_bucket_size        512;
    server_names_hash_max_size        1024;
    server_names_hash_bucket_size     128;
    types_hash_max_size               8192;
    msie_padding                      off;
    server_name_in_redirect           off;

    ##/ Request limits \##
    limit_req_zone                    $binary_remote_addr  zone=gulag:1m   rate=60r/m;

    ##/ Header \##
    more_set_headers                  "Server: dabes 9001";

    ##/ General Options \##
    sendfile                          on;
    server_tokens                     off;
    recursive_error_pages             on;
    ignore_invalid_headers            on;

    ##/ TCP options \##
    tcp_nodelay                       on;
    tcp_nopush                        on;

    ##/ Compression \##
    gzip                              on;
    gzip_disable                      "msie6";
    gzip_static                       on;
    gzip_buffers                      16 8k;
    gzip_comp_level                   3;
    gzip_http_version                 1.0;
    gzip_min_length                   0;
    gzip_vary                         on;
    gzip_proxied                      any;
    gzip_types                        text/plain text/css text/xml text/javascript application/x-javascript application/xml application/xml+rss application/json;

    # Default site access turned off \##
    include   /usr/local/openresty/nginx/conf/global/default_site.conf;

    # PHP Upstream \##
    include   /usr/local/openresty/nginx/conf/global/php_upstream.conf;

    # logging directives \##
    include   /usr/local/openresty/nginx/conf/global/log_directives.conf;

    # Load application site config files dynamicly \##
    include   /home/*/etc/nginx.conf;
    include   /www/*/etc/nginx.conf;
    include   /usr/local/openresty/nginx/conf/global/stats.conf;
}
ZOE

# make folder to store awesome global config files
mkdir /usr/local/openresty/nginx/conf/global/

# Set Openresty global/locations.conf file
cat > /usr/local/openresty/nginx/conf/global/locations.conf <<"ZOG"
# setup some helper location blocks
location = /favicon.ico {
    log_not_found off;
    access_log off;
}

location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
}

# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
location ~ /\. {
    deny all;
}

# Directives to send expires headers and turn off 404 error logging.
location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
    expires 24h;
    log_not_found off;
}
ZOG

# Set Openresty global/logging_remote.conf file
cat > /usr/local/openresty/nginx/conf/global/logging_remote.conf <<"ZOH"
access_log syslog:server=unix:/dev/log,facility=local7,tag=nginx,severity=info main;
error_log syslog:server=unix:/dev/log,facility=local7,tag=nginx,severity=error;
ZOH

# Set Openresty global/php_upstream.conf file
cat > /usr/local/openresty/nginx/conf/global/php_upstream.conf <<"ZOK"
# PHP Upstream
upstream php { 
    server unix:/var/run/php/php7.2-fpm.sock;
}
ZOK

# Set Openresty global/log_direcives.conf file
cat > /usr/local/openresty/nginx/conf/global/log_directives.conf <<"ZOL"
# Main access log
access_log   off;

# Log directives
log_format main '$remote_addr "$remote_user" [$time_local] '
    '"$http_host" [$status] "$request" "$body_bytes_sent" "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for" "$request_time" "$gzip_ratio"';

log_format json_format '{ "@time": "$time_iso8601", '
                    '"@fields": { '
                    '"host": "$remote_addr", '
                    '"user": "$remote_user", '
                    '"status": "$status", '
                    '"request": "$request", '
                    '"size": "$body_bytes_sent", '
                    '"user-agent": "$http_user_agent", '
                    '"forwarded_for": "$http_x_forwarded_for", '
                    '"request_time": "$request_time", '
                    '"bytes_sent": "$body_bytes_sent", '
                    '"referrer": "$http_referer" } }';
ZOL

# Set Openresty global/default_site.conf file
cat > /usr/local/openresty/nginx/conf/global/default_site.conf <<"ZOM"
# default deny server
server {
    listen  *:80 default;
    server_name _;

    location / {
        deny all;
    }
}
ZOM

# Set Openresty global/php.conf file
cat > /usr/local/openresty/nginx/conf/global/php.conf <<"ZOI"
location ~ [^/]\.php(/|$) {
    try_files $uri =404;

    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    if (!-f $document_root$fastcgi_script_name) {
            return 404;
    }

    # Mitigate https://httpoxy.org/ vulnerabilities
    fastcgi_param HTTP_PROXY "";

    include fastcgi_params;

    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_pass php;
}
ZOI

# Set Openresty global/ssl.conf file
cat > /usr/local/openresty/nginx/conf/global/ssl.conf <<"ZOJ"
ssl_protocols TLSv1.2 TLSv1.3;# Requires nginx >= 1.13.0 else use TLSv1.2
ssl_prefer_server_ciphers on;

ssl_dhparam                     /etc/openresty/dhparam.pem; # openssl dhparam -out /etc/nginx/dhparam.pem 4096
ssl_ciphers                     ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve                  secp384r1; # Requires nginx >= 1.1.0

ssl_session_timeout             10m;
ssl_session_cache               shared:SSL:10m;
ssl_buffer_size                 8k;

ssl_session_tickets             off; # Requires nginx >= 1.5.9
ssl_stapling                    on; # Requires nginx >= 1.3.7
ssl_stapling_verify             on; # Requires nginx => 1.3.7

resolver                        1.0.0.1 8.8.4.4 valid=300s;
resolver_timeout                5s;

## verify chain of trust of OCSP response using Root CA and Intermediate certs
ssl_trusted_certificate         /etc/cacert.pem;

# Security Headers
add_header                      Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
add_header                      X-Frame-Options DENY;
add_header                      X-Content-Type-Options nosniff;
add_header                      X-XSS-Protection "1; mode=block";
add_header                      X-Content-Type-Options "nosniff";
add_header                      X-Download-Options "noopen";
ZOJ

# Set php-fpm www.conf pool config file
cat > /etc/php/7.2/fpm/pool.d/www.conf <<"ZOK"
; Start a new pool named 'www'.
; the variable $pool can we used in any directive and will be replaced by the
; pool name ('www' here)
[www]

; Per pool prefix
; It only applies on the following directives:
; - 'access.log'
; - 'slowlog'
; - 'listen' (unixsocket)
; - 'chroot'
; - 'chdir'
; - 'php_values'
; - 'php_admin_values'
; When not set, the global prefix (or /usr) applies instead.
; Note: This directive can also be relative to the global prefix.
; Default Value: none
;prefix = /path/to/pools/$pool

; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
user = openresty
group = openresty

; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
listen = /run/php/php7.2-fpm.sock

; Set listen(2) backlog.
; Default Value: 511 (-1 on FreeBSD and OpenBSD)
listen.backlog = -1

; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server. Many
; BSD-derived systems allow connections regardless of permissions.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = openresty
listen.group = openresty
listen.mode = 06660660

; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
;listen.acl_users =
;listen.acl_groups =

; List of addresses (IPv4/IPv6) of FastCGI clients which are allowed to connect.
; Equivalent to the FCGI_WEB_SERVER_ADDRS environment variable in the original
; PHP FCGI (5.2.2+). Makes sense only with a tcp listening socket. Each address
; must be separated by a comma. If this value is left blank, connections will be
; accepted from any ip address.
; Default Value: any
;listen.allowed_clients = 127.0.0.1

; Specify the nice(2) priority to apply to the pool processes (only if set)
; The value can vary from -19 (highest priority) to 20 (lower priority)
; Note: - It will only work if the FPM master process is launched as root
;       - The pool processes will inherit the master process priority
;         unless it specified otherwise
; Default Value: no set
process.priority = -5

; Choose how the process manager will control the number of child processes.
; Possible Values:
;   static  - a fixed number (pm.max_children) of child processes;
;   dynamic - the number of child processes are set dynamically based on the
;             following directives. With this process management, there will be
;             always at least 1 children.
;             pm.max_children      - the maximum number of children that can
;                                    be alive at the same time.
;             pm.start_servers     - the number of children created on startup.
;             pm.min_spare_servers - the minimum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is less than this
;                                    number then some children will be created.
;             pm.max_spare_servers - the maximum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is greater than this
;                                    number then some children will be killed.
;  ondemand - no children are created at startup. Children will be forked when
;             new requests will connect. The following parameter are used:
;             pm.max_children           - the maximum number of children that
;                                         can be alive at the same time.
;             pm.process_idle_timeout   - The number of seconds after which
;                                         an idle process will be killed.
; Note: This value is mandatory.
pm = dynamic

; The number of child processes to be created when pm is set to 'static' and the
; maximum number of child processes when pm is set to 'dynamic' or 'ondemand'.
; This value sets the limit on the number of simultaneous requests that will be
; served. Equivalent to the ApacheMaxClients directive with mpm_prefork.
; Equivalent to the PHP_FCGI_CHILDREN environment variable in the original PHP
; CGI. The below defaults are based on a server without much resources. Don't
; forget to tweak pm.* to fit your needs.
; Note: Used when pm is set to 'static', 'dynamic' or 'ondemand'
; Note: This value is mandatory.
pm.max_children = 12

; The number of child processes created on startup.
; Note: Used only when pm is set to 'dynamic'
; Default Value: min_spare_servers + (max_spare_servers - min_spare_servers) / 2
pm.start_servers = 4

; The desired minimum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.min_spare_servers = 2

; The desired maximum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.max_spare_servers = 10

; The number of seconds after which an idle process will be killed.
; Note: Used only when pm is set to 'ondemand'
; Default Value: 10s
;pm.process_idle_timeout = 10s;

; The number of requests each child process should execute before respawning.
; This can be useful to work around memory leaks in 3rd party libraries. For
; endless request processing specify '0'. Equivalent to PHP_FCGI_MAX_REQUESTS.
; Default Value: 0
pm.max_requests = 2000

; The URI to view the FPM status page. If this value is not set, no URI will be
; recognized as a status page. It shows the following informations:
;   pool                 - the name of the pool;
;   process manager      - static, dynamic or ondemand;
;   start time           - the date and time FPM has started;
;   start since          - number of seconds since FPM has started;
;   accepted conn        - the number of request accepted by the pool;
;   listen queue         - the number of request in the queue of pending
;                          connections (see backlog in listen(2));
;   max listen queue     - the maximum number of requests in the queue
;                          of pending connections since FPM has started;
;   listen queue len     - the size of the socket queue of pending connections;
;   idle processes       - the number of idle processes;
;   active processes     - the number of active processes;
;   total processes      - the number of idle + active processes;
;   max active processes - the maximum number of active processes since FPM
;                          has started;
;   max children reached - number of times, the process limit has been reached,
;                          when pm tries to start more children (works only for
;                          pm 'dynamic' and 'ondemand');
; Value are updated in real time.
; Example output:
;   pool:                 www
;   process manager:      static
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          62636
;   accepted conn:        190460
;   listen queue:         0
;   max listen queue:     1
;   listen queue len:     42
;   idle processes:       4
;   active processes:     11
;   total processes:      15
;   max active processes: 12
;   max children reached: 0
;
; By default the status page output is formatted as text/plain. Passing either
; 'html', 'xml' or 'json' in the query string will return the corresponding
; output syntax. Example:
;   http://www.foo.bar/status
;   http://www.foo.bar/status?json
;   http://www.foo.bar/status?html
;   http://www.foo.bar/status?xml
;
; By default the status page only outputs short status. Passing 'full' in the
; query string will also return status for each pool process.
; Example:
;   http://www.foo.bar/status?full
;   http://www.foo.bar/status?json&full
;   http://www.foo.bar/status?html&full
;   http://www.foo.bar/status?xml&full
; The Full status returns for each process:
;   pid                  - the PID of the process;
;   state                - the state of the process (Idle, Running, ...);
;   start time           - the date and time the process has started;
;   start since          - the number of seconds since the process has started;
;   requests             - the number of requests the process has served;
;   request duration     - the duration in µs of the requests;
;   request method       - the request method (GET, POST, ...);
;   request URI          - the request URI with the query string;
;   content length       - the content length of the request (only with POST);
;   user                 - the user (PHP_AUTH_USER) (or '-' if not set);
;   script               - the main script called (or '-' if not set);
;   last request cpu     - the %cpu the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because CPU calculation is done when the request
;                          processing has terminated;
;   last request memory  - the max amount of memory the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because memory calculation is done when the request
;                          processing has terminated;
; If the process is in Idle state, then informations are related to the
; last request the process has served. Otherwise informations are related to
; the current request being served.
; Example output:
;   ************************
;   pid:                  31330
;   state:                Running
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          63087
;   requests:             12808
;   request duration:     1250261
;   request method:       GET
;   request URI:          /test_mem.php?N=10000
;   content length:       0
;   user:                 -
;   script:               /home/fat/web/docs/php/test_mem.php
;   last request cpu:     0.00
;   last request memory:  0
;
; Note: There is a real-time FPM status monitoring sample web page available
;       It's available in: /usr/share/php/7.1/fpm/status.html
;
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
pm.status_path = /__stats__234234234_status

; The ping URI to call the monitoring page of FPM. If this value is not set, no
; URI will be recognized as a ping page. This could be used to test from outside
; that FPM is alive and responding, or to
; - create a graph of FPM availability (rrd or such);
; - remove a server from a group if it is not responding (load balancing);
; - trigger alerts for the operating team (24/7).
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
ping.path = /__stats__234234234_ping

ZOK

# Set Openresty global/ssl.conf file
cat > /usr/local/openresty/nginx/conf/global/stats.conf <<"ZOL"
server {
    listen                          80;
    server_name                     www.nginx_service_status.local nginx_service_status.local;

    location ~ ^/(__stats__234234234_status|__stats__234234234_ping)$ {
        access_log off;
        allow 127.0.0.1;
        deny all;
        include fastcgi_params;
        fastcgi_pass php;
    }

    location / {
          stub_status on;
          access_log   off;
          allow 127.0.0.1;
          deny all;
    }

}
ZOL

# PHP-FPM Socket: /run/php/php7.1-fpm.sock

# Install Percona MySQL
apt install -y percona-server-server-5.7
SYSTEM_SERVICES+=('mysql')

# Install Oracle Java 8 (JRE,JDK)
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
apt install -y oracle-java8-installer
apt install -y oracle-java8-set-default

# Install ElasticSearch 1.5
apt-get install -y elasticsearch
SYSTEM_SERVICES+=('elasticsearch')

# Install ElasticSearch 1.5
apt install -y supervisor
SYSTEM_SERVICES+=('supervisor')
sed -i -e "s/files = \/etc\/supervisor\/conf\.d\/\*\.conf/files = \/etc\/supervisor\/conf\.d\/\*\.conf \/www\/\*\/etc\/supervisor\.conf/" /etc/supervisor/supervisord.conf

# Install Yarn
apt install -y yarn

# loop through all enabled system services and set them for startup on boot and start them now
systemctl daemon-reload
for service_name in "${SYSTEM_SERVICES[@]}"
do
    systemctl enable ${service_name}.service
    systemctl start ${service_name}.service
done

# Enable auto renew ssl with certbot
(crontab -l ; echo "0 4 * * * certbot renew --renew-hook \"service openresty reload\"  2>&1") | crontab

# Install NPM modules
yarn global add gulp --prefix /usr/local
yarn global add bower --prefix /usr/local

# Cleanup
cd

openssl dhparam -out /etc/openresty/dhparam.pem 4096

# Setup Automatic Low Priority Stable Updates
dpkg-reconfigure --priority=low unattended-upgrades

# Exit!
exit 0
