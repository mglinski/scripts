#!/usr/bin/env bash

apt-get -y install make gcc build-essential openssl libcurl4-openssl-dev zlib1g zlib1g-dev zlibc libgcrypt11 libgcrypt11-dev wget

cd /root

useradd -m -s /bin/bash ircd

# get latest ircd source code
wget https://www.unrealircd.org/downloads/Unreal3.2.10.5.tar.gz
tar xzvf Unreal3.*.tar.gz && rm Unreal3.*.tar.gz

mkdir /opt
mv /root/Unreal3.2.10.5 /opt/ircd
chown -R ircd:ircd /opt/ts3

cd /opt/ircd

# User config setup
/opt/ircd/Config

# compile source code
make

# do stuff
cp /opt/ircd/doc/example.conf /opt/ircd/unrealircd.conf
touch /opt/ircd/doc/ircd.log

cat > /lib/systemd/system/ircd.service <<SEC2
[Unit]
Description=UnrealIRCd 3 Server
After=network.target

[Service]
WorkingDirectory=/opt/ircd
User=ircd
Group=ircd
Type=forking
ExecStart=/opt/ircd/src/ircd
PIDFile=/opt/ircd/ircd.pid
RestartSec=15
Restart=always

[Install]
WantedBy=multi-user.target
SEC2

systemctl daemon-reload
systemctl enable ircd.service

chown -R ircd:ircd /opt/ircd

nano /opt/ircd/unrealircd.conf