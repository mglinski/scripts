#!/usr/bin/env bash


apt-get install -y sqlite

cd /root

useradd -m -s /bin/bash teamspeak3

wget http://dl.4players.de/ts/releases/3.0.11.4/teamspeak3-server_linux-amd64-3.0.11.4.tar.gz
tar -xzvf teamspeak3-server_linux-amd64-3.0.11.4.tar.gz && rm teamspeak3-server_linux-amd64-3.0.11.4.tar.gz

mkdir /opt
mv /root/teamspeak3-server_linux-amd64 /opt/ts3
chown -R teamspeak3:teamspeak3 /opt/ts3

cat > /lib/systemd/system/teamspeak3.service <<SEC2
[Unit]
Description=TeamSpeak 3 Server
After=network.target

[Service]
WorkingDirectory=/opt/ts3/teamspeak3-server_linux-amd64
User=teamspeak
Group=teamspeak
Type=forking
ExecStartPre=-/bin/sqlite3 -line /opt/ts3/teamspeak3-server_linux-amd64/ts3server.sqlitedb 'pragma integrity_check;'
ExecStartPost=-/bin/sqlite3 -line /opt/ts3/teamspeak3-server_linux-amd64/ts3server.sqlitedb 'vacuum;'
ExecStart=/opt/ts3/teamspeak3-server_linux-amd64/ts3server_startscript.sh start inifile=ts3server.ini
ExecStop=/opt/ts3/teamspeak3-server_linux-amd64/ts3server_startscript.sh stop
PIDFile=/opt/ts3/teamspeak3-server_linux-amd64/ts3server.pid
RestartSec=15
Restart=always

[Install]
WantedBy=multi-user.target
SEC2

systemctl daemon-reload
systemctl enable teamspeak3.service

# generate default INI file
/opt/ts3/ts3server_minimal_runscript.sh createinifile=1
nano ts3server.ini

# service teamspeak3 start