#!/usr/bin/env bash

#######################
# Full Stack Setup
# Debian 8 (Jesse), using systemd
# Openresty(1.11), PHP (7.1), PostgreSql(9.6), Redis(3.3), ElasticSearch(2.1), Percona MongoDB(3.4), Memcached(1.4), Oracle Java 8
#
# Sites go in /www like this:
# /www/(site-fqdn)/         Site Root Folder (site-fqdn: website.com)
# /www/*/etc/nginx.conf     Site nginx conf file
# /www/*/ssl/*              Site specific SSL files/secrets (private key, cert, etc)
# /www/*/logs/              Site specific logs (nginx logs, app logs, etc)
# /www/*/public/*           Site web root folder (default webroot, can be overwritten in etc/nginx.conf)
# /www/*/data/*             Site specific misc data folder(sessions, locks, etc)
# /www/*/tmp/*              Site specific tmp folder (scratch folder that can be written to freely)
#######################

echo "----------------------------------"
echo " Setup server from debian minimal "
echo "----------------------------------"
echo
echo "Please choose some of the options below"
echo "to configure individual components."
echo "(case sensitive)"
echo

echo "Would you like to run this with everything turned on? (don't be stupid though...) [y/n]"
read RUN_DEFAULTS ; echo
if [ "$RUN_DEFAULTS" == "y" ]
then
    TIMEZONE=y
    AUTO_UPDATES=y
else

    echo "Configure timezone as UTC? [y/n]"
    read TIMEZONE ; echo

    if [ "$TIMEZONE" == "y" ]; then
        cp /usr/share/zoneinfo/UTC /etc/localtime
        echo "Timezone is now set to UTC"
    fi

    echo "Configure automatic updates? [y/n]"
    read AUTO_UPDATES ; echo

    echo "Perform a apt-get update && upgrade after setting up this machine? [y/n]"
    read POST_UPDATE ; echo
fi

echo "Configure hostname? [y/n]"
read NEW_HOSTNAME ; echo

if [ "$NEW_HOSTNAME" == "y" ] ; then
	echo "Please input your new hostname: "
	read HOSTNAME_URI ; echo

	# backup original hosts file
	cp /etc/hosts /etc/hosts.back

	ORIG_HOSTNAME=`cat /etc/hostname`
	sed -i '' -e  "s/$ORIG_HOSTNAME/HOSTNAME_URI/" /etc/hosts
	cat ${HOSTNAME_URI} > /etc/hostname
fi

echo "Review your choices above and type 'y' to continue..."
read CONT_INSTALL ; echo

if [ "$CONT_INSTALL" == "y" ] ; then
	echo "Continuing install..."
	echo
else
	echo "Exiting installer."
	exit 0
fi

# Install PHP Repo
apt-key adv --keyserver keys.gnupg.net --recv-keys AC0E47584A7A714D
echo "# PHP Repos
deb http://packages.sury.org/php jessie main
deb-src http://packages.sury.org/php jessie main" > sury-php-jessie.list

# Install Redis Repo
apt-key adv --keyserver keys.gnupg.net --recv-keys C7917B12
echo "# Redis Repos
deb http://ppa.launchpad.net/chris-lea/redis-server/ubuntu trusty main
deb-src http://ppa.launchpad.net/chris-lea/redis-server/ubuntu trusty main" > chris-lea-redis-server-jessie.list

# Install MySQL Repos
apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5
echo "# MySQL Repos
deb http://repo.mysql.com/apt/debian jessie mysql-5.7
deb-src http://repo.mysql.com/apt/debian jessie mysql-5.7" > /etc/apt/sources.list.d/mysql.list

# Install ElasticSearch Repo
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "# Elastic Search
deb http://packages.elastic.co/elasticsearch/2.1/trusty stable main
deb-src http://packages.elastic.co/elasticsearch/2.1/trusty stable main" > /etc/apt/sources.list.d/elasticsearch.list

# Install PostgreSQL Repos
apt-key adv --keyserver pgp.mit.edu --recv-keys ACCC4CF8
echo "# PostgreSQL Repos
deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main
deb-src http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Install Java 8 Repos
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
echo "# Java JRE Repos
deb http://ppa.launchpad.net/webupd8team/java/ubuntu vivid main
deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu vivid main" > /etc/apt/sources.list.d/webupd8team-java.list

# Install MongoDB
apt-key adv --keyserver keys.gnupg.net --recv-keys 1C4CBDCDCD2EFD2A
apt-key adv --keyserver keys.gnupg.net --recv-keys 9334A25F8507EFA5
echo "# Percona MongoDB Repos
deb http://repo.percona.com/apt "$(lsb_release -sc)" main
deb-src http://repo.percona.com/apt "$(lsb_release -sc)" main " | sudo tee /etc/apt/sources.list.d/percona.list

# Array of system services to enable after everything is done
SYSTEM_SERVICES=()

# Increase hashing rounds on system passwords ))))))))))
sed -i -e 's/sha512$/sha512 rounds=65535\n/g' /etc/pam.d/common-password

# Download cacert.pem file from haxxe
wget -O /etc/cacert.pem http://curl.haxx.se/ca/cacert.pem

# Initial system update and package upgrade
apt-get update
apt-get upgrade -y

# Enable unattended debian-security upgrades
apt-get install -y unattended-upgrades bsd-mailx

# install needed base packages
apt-get install -y \
	build-essential \
	fail2ban \
	git curl sudo \
	logrotate \
	libreadline6-dev \
	libncurses5-dev \
	libpcre++-dev \
	libssl-dev \
	libgeoip-dev \
	libxml2-dev \
	libxslt1-dev \
	libgd2-xpm-dev \
	libperl-dev \
	zlib1g-dev \
	libpcre3 \
	libpcre3-dev \
	libgoogle-perftools-dev \
	golang \
	cmake \
	libgd-dev \
	libbz2-dev \
	unzip \
	geoip-database-extra \
	geoip-database \
	geoip-bin \
	libgeoip-dev \
	google-perftools \
    libjemalloc-dev \
    gnupg \
    libcurl4-openssl-dev \
    libperl-dev \
    libgd-dev

# Install certbot for letsencrypt
apt-get install -y certbot -t jessie-backports

# Install OpenResty systemd .service Script
echo '[Unit]
Description=OpenResty - high performance web server
Documentation=http://openresty.org/
After=network.target remote-fs.target nss-lookup.target
 
[Service]
Type=forking
PIDFile=/var/run/openresty.pid
ExecStartPre=/usr/sbin/openresty -t -c /etc/openresty/openresty.conf
ExecStart=/usr/sbin/openresty -c /etc/openresty/openresty.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
 
[Install]
WantedBy=multi-user.target
' > /lib/systemd/system/openresty.service

# Install OpenResty logrotate config
echo '/var/log/openresty/*.log {
        daily
        missingok
        rotate 52
        compress
        delaycompress
        notifempty
        create 640 openresty adm
        sharedscripts
        postrotate
                [ -f /var/run/openresty.pid ] && kill -USR1 `cat /var/run/openresty.pid`
        endscript
}
' > /etc/logrotate.d/openresty

# Install IoJS 3.x and NPM
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y iojs
ln -s `which iojs` /usr/bin/nodejs

# install other services
apt-get install -y redis-server postgresql-9.6 postgresql-client-9.6 postgresql-contrib-9.6 mysql-client memcached

# Add new system services to processing list
SYSTEM_SERVICES+=('postgresql')
SYSTEM_SERVICES+=('redis-server')
SYSTEM_SERVICES+=('memcached')

# Install OpenResty Group
if ! getent group openresty >/dev/null; then
   addgroup --system openresty >/dev/null
fi

# Creating openresty user if it does not exit
if ! getent passwd openresty >/dev/null; then
    adduser \
        --system \
        --disabled-login \
        --ingroup openresty \
        --no-create-home \
        --home /nonexistent \
        --gecos "openresty user" \
        --shell /bin/false \
        openresty  >/dev/null
fi

# Move into root home dir for downloads and building
cd /root

# Get latest openresty version number
curl -s -XGET https://github.com/openresty/openresty/tags | grep tag-name > /tmp/openresty_tag
sed -e 's/<[^>]*>//g' /tmp/openresty_tag > /tmp/openresty_ver
OPENRESTY_VER=`sed -e 's/      v//g' /tmp/openresty_ver | head -n 1` && rm -f /tmp/openresty_*

# Download latest version of openresty
wget http://openresty.org/download/openresty-${OPENRESTY_VER}.tar.gz
tar xf openresty-${OPENRESTY_VER}.tar.gz
cd openresty-${OPENRESTY_VER}

#######################
# Build mod_pagespeed #
#######################

export NPS_VERSION=1.12.34.2
wget https://github.com/pagespeed/ngx_pagespeed/archive/v${NPS_VERSION}-beta.zip
unzip v${NPS_VERSION}-beta.zip
cd ngx_pagespeed-${NPS_VERSION}-beta/
export psol_url=https://dl.google.com/dl/page-speed/psol/${NPS_VERSION}.tar.gz
[ -e scripts/format_binary_url.sh ] && psol_url=$(scripts/format_binary_url.sh PSOL_BINARY_URL)
wget ${psol_url}
tar -xzvf $(basename ${psol_url})
rm $(basename ${psol_url})

##################
# Build pcre-jit #
##################

cd /root/openresty-${OPENRESTY_VER}
# Download PCRE so we can enable JIT compliation
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.40.zip
unzip pcre-8.40.zip


##########################
# Build ginx-rtmp-module #
##########################
cd /root/openresty-${OPENRESTY_VER}
wget https://github.com/arut/nginx-rtmp-module/archive/v1.1.10.zip
unzip v1.1.10.zip


# openresty folders
OPENRESTY_CACHE_PREFIX=/var/cache/openresty
OPENRESTY_LOG_PREFIX=/var/log/openresty

# Make cache folders
mkdir -p ${OPENRESTY_CACHE_PREFIX}/{client_temp,proxy_temp,fastcgi_temp,uwsgi_temp,scgi_temp} ${OPENRESTY_LOG_PREFIX}
chown -R openresty:openresty ${OPENRESTY_CACHE_PREFIX} ${OPENRESTY_LOG_PREFIX}

# Get number of processing threads available to the system
PROCS=`nproc`

# Configure OpenResty
./configure \
    --with-ipv6 \
    --prefix=/etc/openresty \
    --sbin-path=/usr/sbin/openresty \
    --conf-path=/etc/openresty/openresty.conf \
    --error-log-path=${OPENRESTY_LOG_PREFIX}/error.log \
    --http-log-path=${OPENRESTY_LOG_PREFIX}/access.log \
    --pid-path=/var/run/openresty.pid \
    --lock-path=/var/run/openresty.lock \
    --http-client-body-temp-path=${OPENRESTY_CACHE_PREFIX}/client_temp \
    --http-proxy-temp-path=${OPENRESTY_CACHE_PREFIX}/proxy_temp \
    --http-fastcgi-temp-path=${OPENRESTY_CACHE_PREFIX}/fastcgi_temp \
    --http-uwsgi-temp-path=${OPENRESTY_CACHE_PREFIX}/uwsgi_temp \
    --http-scgi-temp-path=${OPENRESTY_CACHE_PREFIX}/scgi_temp \
    --with-cc-opt="-g -O2 -m64 -mtune=native -fPIE -fstack-protector-all --param=ssp-buffer-size=4 -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security" \
    --with-ld-opt="-lrt -ljemalloc -Wl,-Bsymbolic-functions -Wl,-rpath,/usr/local/lib -Wl,-z,relro" \
    --user=openresty \
    --group=openresty \
    --add-module=/root/openresty-${OPENRESTY_VER}/ngx_pagespeed-${NPS_VERSION}-beta/ \
    --add-module=/root/openresty-${OPENRESTY_VER}/nginx-rtmp-module-1.1.10 \
    --with-file-aio \
    --with-ipv6 \
    --with-luajit \
    --with-mail \
    --with-threads \
    --with-stream \
	--with-stream_ssl_module \
    --with-http_v2_module \
    --with-http_realip_module \
    --with-http_addition_module \
    --with-http_xslt_module \
    --with-http_image_filter_module \
    --with-http_geoip_module \
    --with-http_sub_module \
    --with-http_mp4_module \
    --with-http_flv_module \
    --with-http_iconv_module \
    --with-http_gzip_static_module \
    --with-http_random_index_module \
    --with-http_secure_link_module \
    --with-http_degradation_module \
    --with-http_stub_status_module \
    --with-http_ssl_module \
    --with-pcre=pcre-8.40 \
    --with-pcre-jit \
    -j${PROCS}

# Fix some other build errors caused by nginx expecting OpenSSL
cd /root/openresty-${OPENRESTY_VER}/bundle/nginx-1.11.2
patch -p1 < "../../patches/openresty-nginx-boringssl.patch"
cd /root/openresty-${OPENRESTY_VER}

# Install OpenResty
make -j${PROCS}
make install

# setup system error log for openresty
mkdir -p /var/log/openresty/
chown -R openresty:openresty /var/log/openresty/

# add to system services loop
SYSTEM_SERVICES+=('openresty')

# back dat ass up
cd 

# Install PHP + Modules
apt-get install -y php7.1 php7.1-dev php7.1-cli php7.1-cgi php7.1-curl php7.1-common php7.1-fpm php7.1-json php7.1-opcache php7.1-mbstring php7.1-xml php7.1-mysql php7.1-pgsql php7.1-sqlite3 php7.1-intl php7.1-gmp php7.1-igbinary php7.1-memcached php7.1-msgpack php7.1-odbc php7.1-redis

# Install php-libsodium
apt-get -y install libsodium-dev
git clone https://github.com/jedisct1/libsodium-php
cd libsodium-php
phpize7.0
./configure
make && make install
cd
rm -rf libsodium-php
cat > /etc/php/mods-available/libsodium.ini <<"PHPD"
; configuration for php libsodium module
; priority=20
extension=libsodium.so
PHPD
ln -s /etc/php/mods-available/libsodium.ini /etc/php/7.1/cli/conf.d/20-libsodium.ini
ln -s /etc/php/mods-available/libsodium.ini /etc/php/7.1/fpm/conf.d/20-libsodium.ini

# Install ImageMagick
apt-get install libmagickwand-dev
apt-get install libmagickcore-dev
echo "\n" | pecl install imagick

cat > /etc/php/mods-available/imagick.ini <<"PHPE"
; configuration for php imagick module
; priority=20
extension=imagick.so
PHPE

ln -s /etc/php/mods-available/imagick.ini /etc/php/7.1/cli/conf.d/20-imagick.ini
ln -s /etc/php/mods-available/imagick.ini /etc/php/7.1/fpm/conf.d/20-imagick.ini

# add to system services loop
SYSTEM_SERVICES+=('php7.1-fpm')

# Install composer globally
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
cp composer.phar /usr/bin/composer

# Install PHP OpCache Settings
cat > /etc/php/mods-available/opcache.ini <<"ZOA"
; configuration for php ZendOpcache module
; priority=05
zend_extension=opcache.so
opcache.enable=1
opcache.save_comments=1
opcache.enable_file_override=1
opcache.memory_consumption=512
opcache.interned_strings_buffer=256
opcache.max_accelerated_files=512000
opcache.revalidate_freq=0
opcache.validate_timestamps=0
opcache.fast_shutdown=1
opcache.enable_cli=0
ZOA

# Set PHP.ini Settings for CLI
cat >> /etc/php/7.1/fpm/php.ini <<"ZOB"

max_execution_time = 3000
max_input_time = 6000
expose_php = Off
short_open_tag = Off
zlib.output_compression = Off
implicit_flush = Off
memory_limit = 512M
error_reporting = E_ALL
display_errors = Off
log_errors = On
error_log = php_errors.log
post_max_size = 2G
cgi.force_redirect = 1
cgi.fix_pathinfo = 0
upload_max_filesize = 2G
max_file_uploads = 10
date.timezone = UTC
#error_log = __php-fpm.log
ZOB

# Set PHP.ini Settings for FPM
cat >> /etc/php/7.1/cli/php.ini <<"ZOB1"

max_execution_time = 3000
max_input_time = 6000
expose_php = Off
short_open_tag = Off
zlib.output_compression = Off
implicit_flush = Off
memory_limit = 512M
error_reporting = E_ALL
display_errors = Off
log_errors = On
error_log = php_errors.log
post_max_size = 2G
cgi.force_redirect = 1
cgi.fix_pathinfo = 0
upload_max_filesize = 2G
max_file_uploads = 10
date.timezone = UTC
#error_log = __php-fpm.log
ZOB1

# Change listen mode in php-fpm socket
sed -i 's/^;listen.mode = */listen.mode = 0666/' /etc/php/7.1/fpm/pool.d/www.conf

# Set Openresty fastcgi_params file
cat > /etc/openresty/fastcgi_params <<"ZOC"

fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;
fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
fastcgi_param  PATH_INFO          $fastcgi_script_name;
#fastcgi_param   PATH_TRANSLATED $document_root$fastcgi_path_info;

fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;
fastcgi_param  HTTPS              $https if_not_empty;

fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;

fastcgi_connect_timeout 300;
fastcgi_send_timeout 300;
fastcgi_read_timeout 300;
fastcgi_buffer_size 32k;
fastcgi_buffers 32 16k;
fastcgi_busy_buffers_size 256k;
fastcgi_temp_file_write_size 256k;
fastcgi_intercept_errors on;

# PHP only, required if PHP was built with --enable-force-cgi-redirect
#fastcgi_param  REDIRECT_STATUS    200;
ZOC

# Set Openresty main config file
cat > /etc/openresty/openresty.conf <<"ZOE"
###########################
##   Openresty v1.11.x   ##
##     ~All~ Features    ##
###########################

##/ Main thread settings \##
user  openresty;
worker_processes  auto;
worker_priority      15; 
worker_rlimit_nofile 7000000;

##/ PCRE JIT compiler for regex \##
pcre_jit                 on;

##/ FileRead Threadpool \##
thread_pool mainpool threads=632 max_queue=65536; # added in 1.7.11

## Main event loop \##
events {
    use epoll;
    worker_connections  10240;
}

##/ Global HTTP Options \##
http {
    include       mime.types;
    default_type  application/octet-stream;

    ##/ SSL: Globally Mitigate Poodle Attacks \##
    ssl_protocols                     TLSv1 TLSv1.1 TLSv1.2;

    ##/ Cache \##
    add_header                        Cache-Control "public, max-age=3153600";
    client_max_body_size              64M;
    client_body_temp_path             /var/cache/openresty 1 2;
    open_file_cache                   max=1000 inactive=2h;
    open_file_cache_errors            on;
    open_file_cache_min_uses          1;
    open_file_cache_valid             1h;

    ##/ Timeouts \##
    send_timeout                      5;
    keepalive_timeout                 5 5;
    client_body_timeout               5;
    client_header_timeout             5;

    ##/ OpenResty \##
    variables_hash_max_size           4096;
    variables_hash_bucket_size        512;
    server_names_hash_bucket_size     64;
    types_hash_max_size               8192;
    msie_padding                      off;
    server_name_in_redirect           off;

    ##/ Request limits \##
    limit_req_zone                    $binary_remote_addr  zone=gulag:1m   rate=60r/m;

    ##/ Header \##
    more_set_headers                  "Server: dabes 9001";

    ##/ General Options \##
    sendfile                          on;
    server_tokens                     off;
    recursive_error_pages             on;
    ignore_invalid_headers            on;

    ##/ TCP options \##
    tcp_nodelay                       on;
    tcp_nopush                        on;

    ##/ Compression \##
    gzip                              on;
    gzip_disable                      "msie6";
    gzip_static                       on;
    gzip_buffers                      16 8k;
    gzip_comp_level                   3;
    gzip_http_version                 1.0;
    gzip_min_length                   0;
    gzip_vary                         on;
    gzip_proxied                      any;
    gzip_types                        text/plain text/css text/xml text/javascript application/x-javascript application/xml application/xml+rss application/json;

    # Default site access turned off \##
    include   /etc/openresty/global/default_site.conf;

    # PHP Upstream \##
    include   /etc/openresty/global/php_upstream.conf;

    # logging directives \##
    include   /etc/openresty/global/log_directives.conf;

    # Load application site config files dynamicly \##
    include   /home/*/etc/nginx.conf;
    include   /www/*/etc/nginx.conf;
    include   /etc/openresty/global/stats.conf;
}
ZOE

# make folder to store awesome global config files
mkdir /etc/openresty/global/

# Set Openresty global/locations.conf file
cat > /etc/openresty/global/locations.conf <<"ZOG"
# setup some helper location blocks
location = /favicon.ico {
    log_not_found off;
    access_log off;
}

location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
}

# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
location ~ /\. {
    deny all;
}

# Directives to send expires headers and turn off 404 error logging.
location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
    expires 24h;
    log_not_found off;
}
ZOG

# Set Openresty global/logging_remote.conf file
cat > /etc/openresty/global/logging_remote.conf <<"ZOH"
access_log syslog:server=unix:/dev/log,facility=local7,tag=nginx,severity=info main;
error_log syslog:server=unix:/dev/log,facility=local7,tag=nginx,severity=error;
ZOH

# Set Openresty global/php_upstream.conf file
cat > /etc/openresty/global/php_upstream.conf <<"ZOK"
# PHP Upstream
upstream php { 
    server unix:/var/run/php/php7.1-fpm.sock;
}
ZOK

# Set Openresty global/log_direcives.conf file
cat > /etc/openresty/global/log_directives.conf <<"ZOL"
# Main access log
access_log   off;

# Log directives
log_format main '$remote_addr "$remote_user" [$time_local] '
    '"$http_host" [$status] "$request" "$body_bytes_sent" "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for" "$request_time" "$gzip_ratio"';

log_format json_format '{ "@time": "$time_iso8601", '
                    '"@fields": { '
                    '"host": "$remote_addr", '
                    '"user": "$remote_user", '
                    '"status": "$status", '
                    '"request": "$request", '
                    '"size": "$body_bytes_sent", '
                    '"user-agent": "$http_user_agent", '
                    '"forwarded_for": "$http_x_forwarded_for", '
                    '"request_time": "$request_time", '
                    '"bytes_sent": "$body_bytes_sent", '
                    '"referrer": "$http_referer" } }';

log_format main_eve '$remote_addr "$remote_user" [$time_local] '
    '"$http_host" [$status] "$request" "$body_bytes_sent" "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for" "$request_time" "$gzip_ratio"'
    '"$http_eve_charname" "$http_eve_charid" "$http_eve_corpid" "$http_eve_solarsystemid" "$http_eve_stationid" ';
ZOL

# Set Openresty global/default_site.conf file
cat > /etc/openresty/global/default_site.conf <<"ZOM"
# default deny server
server {
    listen  *:80 default;
    server_name _;

    location / {
        deny all;
    }
}
ZOM

# Set Openresty global/php.conf file
cat > /etc/openresty/global/php.conf <<"ZOI"
location ~ [^/]\.php(/|$) {
    try_files $uri =404;

    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    if (!-f $document_root$fastcgi_script_name) {
            return 404;
    }

    # Mitigate https://httpoxy.org/ vulnerabilities
    fastcgi_param HTTP_PROXY "";

    include fastcgi_params;
    #include fastcgi_eve;

    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_pass php;
}
ZOI

# Set Openresty global/ssl.conf file
cat > /etc/openresty/global/ssl.conf <<"ZOJ"
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA; # OpenSSL Cyphers
#ssl_ciphers [ECDHE-ECDSA-AES128-GCM-SHA256|ECDHE-ECDSA-CHACHA20-POLY1305]:[ECDHE-RSA-AES128-GCM-SHA256|ECDHE-RSA-CHACHA20-POLY1305]:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:AES128-GCM-SHA256:AES256-GCM-SHA384; # BoringSSL Cyphers

ssl_buffer_size                 8k;
ssl_session_timeout             10m;
ssl_session_cache               shared:SSL:10m;

ssl_stapling                    on;
ssl_stapling_verify             on;
ssl_session_tickets             off;

## verify chain of trust of OCSP response using Root CA and Intermediate certs
ssl_trusted_certificate         /etc/cacert.pem;

resolver                        8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout                10s;

# Security Headers
add_header                      Strict-Transport-Security max-age=15768000;
add_header                      X-Frame-Options DENY;
add_header                      X-Content-Type-Options nosniff;
add_header                      X-XSS-Protection "1; mode=block";
add_header                      X-Content-Type-Options "nosniff";
add_header                      X-Download-Options "noopen";
ZOJ

# Set php-fpm www.conf pool config file
cat > /etc/php/7.1/fpm/pool.d/www.conf <<"ZOK"
; Start a new pool named 'www'.
; the variable $pool can we used in any directive and will be replaced by the
; pool name ('www' here)
[www]

; Per pool prefix
; It only applies on the following directives:
; - 'access.log'
; - 'slowlog'
; - 'listen' (unixsocket)
; - 'chroot'
; - 'chdir'
; - 'php_values'
; - 'php_admin_values'
; When not set, the global prefix (or /usr) applies instead.
; Note: This directive can also be relative to the global prefix.
; Default Value: none
;prefix = /path/to/pools/$pool

; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
user = openresty
group = openresty

; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
listen = /run/php/php7.1-fpm.sock

; Set listen(2) backlog.
; Default Value: 511 (-1 on FreeBSD and OpenBSD)
listen.backlog = -1

; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server. Many
; BSD-derived systems allow connections regardless of permissions.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = openresty
listen.group = openresty
listen.mode = 06660660

; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
;listen.acl_users =
;listen.acl_groups =

; List of addresses (IPv4/IPv6) of FastCGI clients which are allowed to connect.
; Equivalent to the FCGI_WEB_SERVER_ADDRS environment variable in the original
; PHP FCGI (5.2.2+). Makes sense only with a tcp listening socket. Each address
; must be separated by a comma. If this value is left blank, connections will be
; accepted from any ip address.
; Default Value: any
;listen.allowed_clients = 127.0.0.1

; Specify the nice(2) priority to apply to the pool processes (only if set)
; The value can vary from -19 (highest priority) to 20 (lower priority)
; Note: - It will only work if the FPM master process is launched as root
;       - The pool processes will inherit the master process priority
;         unless it specified otherwise
; Default Value: no set
process.priority = -5

; Choose how the process manager will control the number of child processes.
; Possible Values:
;   static  - a fixed number (pm.max_children) of child processes;
;   dynamic - the number of child processes are set dynamically based on the
;             following directives. With this process management, there will be
;             always at least 1 children.
;             pm.max_children      - the maximum number of children that can
;                                    be alive at the same time.
;             pm.start_servers     - the number of children created on startup.
;             pm.min_spare_servers - the minimum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is less than this
;                                    number then some children will be created.
;             pm.max_spare_servers - the maximum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is greater than this
;                                    number then some children will be killed.
;  ondemand - no children are created at startup. Children will be forked when
;             new requests will connect. The following parameter are used:
;             pm.max_children           - the maximum number of children that
;                                         can be alive at the same time.
;             pm.process_idle_timeout   - The number of seconds after which
;                                         an idle process will be killed.
; Note: This value is mandatory.
pm = dynamic

; The number of child processes to be created when pm is set to 'static' and the
; maximum number of child processes when pm is set to 'dynamic' or 'ondemand'.
; This value sets the limit on the number of simultaneous requests that will be
; served. Equivalent to the ApacheMaxClients directive with mpm_prefork.
; Equivalent to the PHP_FCGI_CHILDREN environment variable in the original PHP
; CGI. The below defaults are based on a server without much resources. Don't
; forget to tweak pm.* to fit your needs.
; Note: Used when pm is set to 'static', 'dynamic' or 'ondemand'
; Note: This value is mandatory.
pm.max_children = 12

; The number of child processes created on startup.
; Note: Used only when pm is set to 'dynamic'
; Default Value: min_spare_servers + (max_spare_servers - min_spare_servers) / 2
pm.start_servers = 4

; The desired minimum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.min_spare_servers = 2

; The desired maximum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.max_spare_servers = 10

; The number of seconds after which an idle process will be killed.
; Note: Used only when pm is set to 'ondemand'
; Default Value: 10s
;pm.process_idle_timeout = 10s;

; The number of requests each child process should execute before respawning.
; This can be useful to work around memory leaks in 3rd party libraries. For
; endless request processing specify '0'. Equivalent to PHP_FCGI_MAX_REQUESTS.
; Default Value: 0
pm.max_requests = 2000

; The URI to view the FPM status page. If this value is not set, no URI will be
; recognized as a status page. It shows the following informations:
;   pool                 - the name of the pool;
;   process manager      - static, dynamic or ondemand;
;   start time           - the date and time FPM has started;
;   start since          - number of seconds since FPM has started;
;   accepted conn        - the number of request accepted by the pool;
;   listen queue         - the number of request in the queue of pending
;                          connections (see backlog in listen(2));
;   max listen queue     - the maximum number of requests in the queue
;                          of pending connections since FPM has started;
;   listen queue len     - the size of the socket queue of pending connections;
;   idle processes       - the number of idle processes;
;   active processes     - the number of active processes;
;   total processes      - the number of idle + active processes;
;   max active processes - the maximum number of active processes since FPM
;                          has started;
;   max children reached - number of times, the process limit has been reached,
;                          when pm tries to start more children (works only for
;                          pm 'dynamic' and 'ondemand');
; Value are updated in real time.
; Example output:
;   pool:                 www
;   process manager:      static
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          62636
;   accepted conn:        190460
;   listen queue:         0
;   max listen queue:     1
;   listen queue len:     42
;   idle processes:       4
;   active processes:     11
;   total processes:      15
;   max active processes: 12
;   max children reached: 0
;
; By default the status page output is formatted as text/plain. Passing either
; 'html', 'xml' or 'json' in the query string will return the corresponding
; output syntax. Example:
;   http://www.foo.bar/status
;   http://www.foo.bar/status?json
;   http://www.foo.bar/status?html
;   http://www.foo.bar/status?xml
;
; By default the status page only outputs short status. Passing 'full' in the
; query string will also return status for each pool process.
; Example:
;   http://www.foo.bar/status?full
;   http://www.foo.bar/status?json&full
;   http://www.foo.bar/status?html&full
;   http://www.foo.bar/status?xml&full
; The Full status returns for each process:
;   pid                  - the PID of the process;
;   state                - the state of the process (Idle, Running, ...);
;   start time           - the date and time the process has started;
;   start since          - the number of seconds since the process has started;
;   requests             - the number of requests the process has served;
;   request duration     - the duration in µs of the requests;
;   request method       - the request method (GET, POST, ...);
;   request URI          - the request URI with the query string;
;   content length       - the content length of the request (only with POST);
;   user                 - the user (PHP_AUTH_USER) (or '-' if not set);
;   script               - the main script called (or '-' if not set);
;   last request cpu     - the %cpu the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because CPU calculation is done when the request
;                          processing has terminated;
;   last request memory  - the max amount of memory the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because memory calculation is done when the request
;                          processing has terminated;
; If the process is in Idle state, then informations are related to the
; last request the process has served. Otherwise informations are related to
; the current request being served.
; Example output:
;   ************************
;   pid:                  31330
;   state:                Running
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          63087
;   requests:             12808
;   request duration:     1250261
;   request method:       GET
;   request URI:          /test_mem.php?N=10000
;   content length:       0
;   user:                 -
;   script:               /home/fat/web/docs/php/test_mem.php
;   last request cpu:     0.00
;   last request memory:  0
;
; Note: There is a real-time FPM status monitoring sample web page available
;       It's available in: /usr/share/php/7.1/fpm/status.html
;
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
pm.status_path = /__stats__234234234_status

; The ping URI to call the monitoring page of FPM. If this value is not set, no
; URI will be recognized as a ping page. This could be used to test from outside
; that FPM is alive and responding, or to
; - create a graph of FPM availability (rrd or such);
; - remove a server from a group if it is not responding (load balancing);
; - trigger alerts for the operating team (24/7).
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
ping.path = /__stats__234234234_ping

; This directive may be used to customize the response of a ping request. The
; response is formatted as text/plain with a 200 response code.
; Default Value: pong
;ping.response = pong

; The access log file
; Default: not set
;access.log = log/$pool.access.log

; The access log format.
; The following syntax is allowed
;  %%: the '%' character
;  %C: %CPU used by the request
;      it can accept the following format:
;      - %{user}C for user CPU only
;      - %{system}C for system CPU only
;      - %{total}C  for user + system CPU (default)
;  %d: time taken to serve the request
;      it can accept the following format:
;      - %{seconds}d (default)
;      - %{miliseconds}d
;      - %{mili}d
;      - %{microseconds}d
;      - %{micro}d
;  %e: an environment variable (same as $_ENV or $_SERVER)
;      it must be associated with embraces to specify the name of the env
;      variable. Some exemples:
;      - server specifics like: %{REQUEST_METHOD}e or %{SERVER_PROTOCOL}e
;      - HTTP headers like: %{HTTP_HOST}e or %{HTTP_USER_AGENT}e
;  %f: script filename
;  %l: content-length of the request (for POST request only)
;  %m: request method
;  %M: peak of memory allocated by PHP
;      it can accept the following format:
;      - %{bytes}M (default)
;      - %{kilobytes}M
;      - %{kilo}M
;      - %{megabytes}M
;      - %{mega}M
;  %n: pool name
;  %o: output header
;      it must be associated with embraces to specify the name of the header:
;      - %{Content-Type}o
;      - %{X-Powered-By}o
;      - %{Transfert-Encoding}o
;      - ....
;  %p: PID of the child that serviced the request
;  %P: PID of the parent of the child that serviced the request
;  %q: the query string
;  %Q: the '?' character if query string exists
;  %r: the request URI (without the query string, see %q and %Q)
;  %R: remote IP address
;  %s: status (response code)
;  %t: server time the request was received
;      it can accept a strftime(3) format:
;      %d/%b/%Y:%H:%M:%S %z (default)
;      The strftime(3) format must be encapsuled in a %{<strftime_format>}t tag
;      e.g. for a ISO8601 formatted timestring, use: %{%Y-%m-%dT%H:%M:%S%z}t
;  %T: time the log has been written (the request has finished)
;      it can accept a strftime(3) format:
;      %d/%b/%Y:%H:%M:%S %z (default)
;      The strftime(3) format must be encapsuled in a %{<strftime_format>}t tag
;      e.g. for a ISO8601 formatted timestring, use: %{%Y-%m-%dT%H:%M:%S%z}t
;  %u: remote user
;
; Default: "%R - %u %t \"%m %r\" %s"
;access.format = "%R - %u %t \"%m %r%Q%q\" %s %f %{mili}d %{kilo}M %C%%"

; The log file for slow requests
; Default Value: not set
; Note: slowlog is mandatory if request_slowlog_timeout is set
;slowlog = log/$pool.log.slow

; The timeout for serving a single request after which a PHP backtrace will be
; dumped to the 'slowlog' file. A value of '0s' means 'off'.
; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
; Default Value: 0
;request_slowlog_timeout = 0

; The timeout for serving a single request after which the worker process will
; be killed. This option should be used when the 'max_execution_time' ini option
; does not stop script execution for some reason. A value of '0' means 'off'.
; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
; Default Value: 0
;request_terminate_timeout = 0

; Set open file descriptor rlimit.
; Default Value: system defined value
;rlimit_files = 1024

; Set max core size rlimit.
; Possible Values: 'unlimited' or an integer greater or equal to 0
; Default Value: system defined value
;rlimit_core = 0

; Chroot to this directory at the start. This value must be defined as an
; absolute path. When this value is not set, chroot is not used.
; Note: you can prefix with '$prefix' to chroot to the pool prefix or one
; of its subdirectories. If the pool prefix is not set, the global prefix
; will be used instead.
; Note: chrooting is a great security feature and should be used whenever
;       possible. However, all PHP paths will be relative to the chroot
;       (error_log, sessions.save_path, ...).
; Default Value: not set
;chroot =

; Chdir to this directory at the start.
; Note: relative path can be used.
; Default Value: current directory or / when chroot
;chdir = /var/www

; Redirect worker stdout and stderr into main error log. If not set, stdout and
; stderr will be redirected to /dev/null according to FastCGI specs.
; Note: on highloaded environement, this can cause some delay in the page
; process time (several ms).
; Default Value: no
;catch_workers_output = yes

; Clear environment in FPM workers
; Prevents arbitrary environment variables from reaching FPM worker processes
; by clearing the environment in workers before env vars specified in this
; pool configuration are added.
; Setting to "no" will make all environment variables available to PHP code
; via getenv(), $_ENV and $_SERVER.
; Default Value: yes
;clear_env = no

; Limits the extensions of the main script FPM will allow to parse. This can
; prevent configuration mistakes on the web server side. You should only limit
; FPM to .php extensions to prevent malicious users to use other extensions to
; exectute php code.
; Note: set an empty value to allow all extensions.
; Default Value: .php
;security.limit_extensions = .php .php3 .php4 .php5 .php7

; Pass environment variables like LD_LIBRARY_PATH. All $VARIABLEs are taken from
; the current environment.
; Default Value: clean env
;env[HOSTNAME] = $HOSTNAME
;env[PATH] = /usr/local/bin:/usr/bin:/bin
;env[TMP] = /tmp
;env[TMPDIR] = /tmp
;env[TEMP] = /tmp

; Additional php.ini defines, specific to this pool of workers. These settings
; overwrite the values previously defined in the php.ini. The directives are the
; same as the PHP SAPI:
;   php_value/php_flag             - you can set classic ini defines which can
;                                    be overwritten from PHP call 'ini_set'.
;   php_admin_value/php_admin_flag - these directives won't be overwritten by
;                                     PHP call 'ini_set'
; For php_*flag, valid values are on, off, 1, 0, true, false, yes or no.

; Defining 'extension' will load the corresponding shared extension from
; extension_dir. Defining 'disable_functions' or 'disable_classes' will not
; overwrite previously defined php.ini values, but will append the new value
; instead.

; Note: path INI options can be relative and will be expanded with the prefix
; (pool, global or /usr)

; Default Value: nothing is defined by default except the values in php.ini and
;                specified at startup with the -d argument
;php_admin_value[sendmail_path] = /usr/sbin/sendmail -t -i -f www@my.domain.com
;php_flag[display_errors] = off
;php_admin_value[error_log] = /var/log/fpm-php.www.log
;php_admin_flag[log_errors] = on
;php_admin_value[memory_limit] = 32M
ZOK

# Set Openresty global/ssl.conf file
cat > /etc/openresty/global/stats.conf <<"ZOL"
server {
    listen                          80;
    server_name                     www.nginx_service_status.local nginx_service_status.local;

    location ~ ^/(__stats__234234234_status|__stats__234234234_ping)$ {
        access_log off;
        allow 127.0.0.1;
        deny all;
        include fastcgi_params;
        fastcgi_pass php;
    }

    location / {
          stub_status on;
          access_log   off;
          allow 127.0.0.1;
          deny all;
    }

}
ZOL

# PHP-FPM Socket: /run/php/php7.1-fpm.sock

# Install Percona MongoDB
apt-get install -y percona-server-mongodb
SYSTEM_SERVICES+=('mongod')

# Install Oracle Java 8 (JRE,JDK)
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
apt-get install -y oracle-java8-installer
apt-get install -y oracle-java8-set-default

# Install ElasticSearch 1.5
apt-get install -y elasticsearch
SYSTEM_SERVICES+=('elasticsearch')

# install iojs
# curl https://raw.githubusercontent.com/creationix/nvm/v0.24.1/install.sh | bash
# nvm install iojs

# loop through all enabled system services and set them for startup on boot and start them now
for service_name in "${SYSTEM_SERVICES[@]}"
do
    systemctl enable ${service_name}.service
    systemctl start ${service_name}.service
done

# Enable auto renew ssl with certbot
(crontab -l ; echo "0 4 * * * certbot renew -q 2>&1") | crontab

# Install NPM modules
npm install -g gulp bower gulp

# Cleanup
cd
rm -rf ./openresty-${OPENRESTY_VER}

# Setup Automatic Low Priority Stable Updates
dpkg-reconfigure --priority=low -y unattended-upgrades

# Exit!
exit 0
