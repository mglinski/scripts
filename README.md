## System Scripts

Some of my system install scripts, mainly for Debian. Updated semi-frequently.

### Debian 

Includes system setup scriptes for installing this software stack in debian servers using active and popular repos wherever possible:

* [Openresty](http://openresty.org/) (latest) [Nginx custom configuration with LUA scripting capabilities]
* [PHP](http://php.net) (latest) [Popular web scripting/programming language, php-fpm, extra modules]
* [Percona MySQL Server](https://www.percona.com/software/mongo-database/percona-server-for-mongodb) (v5.7, latest update) [MySQL Server + Additions]
* [Percona XtraBackupDB](https://www.percona.com/software/mongo-database/percona-server-for-mongodb) (v3.4, latest update) MySQL BackupToolchain + Additions]
* [Percona Toolkit](https://www.percona.com/software/database-tools/percona-toolkit) (v3.0, latest update) MySQL DBA Tools]
* [Redis](http://redis.io) (latest) [KVS Database with helpful datatypes and functionality]
* [ElasticSearch](https://www.elastic.co/products/elasticsearch) (latest) [FullText Supported Document Index and Warehouse]
* [Memcached](http://memcached.org/) (latest) [In Memory Data Blob cache]
* [Oracle Java](https://www.oracle.com/java/index.html) (v8.x, latest update) [Java Programming Language, needed for ElasticSearch]
* [Percona MongoDB Server](https://www.percona.com/software/mongo-database/percona-server-for-mongodb) (v3.4, latest update) [MongoDB Server + Additions]

* [Percona MongoDB Server](https://www.percona.com/software/mongo-database/percona-server-for-mongodb) (v3.4, latest update) [MongoDB Server + Additions]

Some platform specific notes below:

#### 9 (Stretch)

* Most up to date build system
* Full support for systemd
* Openresty built with Google Pagespeed
* Openresty compiler options to enable ASLR, -O2 Optimizations, Stack Protection and Linker protection

#### 8 (Jessie)

* Full support for systemd
* Openresty built with Google Pagespeed
* Openresty compiler options to enable ASLR, -O2 Optimizations, Stack Protection and Linker protection

## License 

MIT Licensed

## Copyright

(c) 2015 Matthew Glinski
